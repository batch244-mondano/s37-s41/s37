const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Course = require("../models/Course");

// checks if the email already exists
/*
	Steps:
	1. use "mongoose" "find method" to find duplicate emails
	2. .then method to send a response back to the frontend application based on the result of the find method
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		// find method returns a record if a match is found
		if(result.length > 0){
			return true;

		// no duplicate email found
		// the user is not yet registered in the database
		} else {
			return false;
		}
	})
}

// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (reqBody)=>{
	// creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// uses the information from the request body to provide all the necessary information
    let newUser = new User ({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,

        // Syntax: bcrypt.hashSync(dataToBeEncrypted), salt)
        password : bcrypt.hashSync(reqBody.password, 10)
    })
//saves the created objects to our database
    return newUser.save().then((user,error)=>{
        // user registration failed
        if (error){
            return false;

        // User registration successful
        }else{
            return true;
        }

    })
}


// User authentication
	/*
		Steps:
		1. Check the database if the user email exists
		2. Compare the password provided in the login form with the password stored in the database
		3. Generate/return a JSON web token if the user is successfully logged in and return false if not
	*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false;
		} else {
			// Creates a variable to return the result of comparing the log-in form password and the database password
			// "compareSync" is used to compare a non encrypted password from the log-in to the encrypted password from the database and returns "true" or "false"
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// if the password matches
			if(isPasswordCorrect) {

				// generates an access token
				// it uses the "createAccessToken" method that we defined in the auth.js file
				// returning an object
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
}

// s38 activity - retrieve details of a user

module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.userId).then((result,error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			result.password = "";
			return result;
		}
	});
};

// Enroll a user to a course
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/

/*
module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId : data.courseId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId})

		return course.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	});

	if (isUserUpdated && isCourseUpdated) {
		return true;
	} else {
		return false;
	}
};
*/

// S41 Activity
module.exports.enroll = async (data) => {
  let isUserUpdated = await User.findById(data.userId);
  let isCourseUpdated = await Course.findById(data.courseId);
  let isEnrolled = isCourseUpdated.enrollees.find(
    (user) => user.userId == data.userId );

  if (isUserUpdated && isCourseUpdated && !isEnrolled) {
        isUserUpdated.enrollments.push({ courseId: data.courseId });
        isUserUpdated.save();
        isCourseUpdated.enrollees.push({ userId: data.userId });
        isCourseUpdated.save();
    return true;
  } else {
    return false;
  }
};
